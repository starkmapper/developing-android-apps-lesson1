package nl.mapper.sunshine.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

import nl.mapper.sunshine.R;

/**
 * Draws an arc on the screen.
 */
public class ArcView extends View
{
	public ArcView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		init();
	}

	public ArcView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public ArcView(Context context)
	{
		super(context);
		init();
	}

	private Paint arrowPaint;
	private Paint arcPaint;
	private Path arrowPath;
	private void init()
	{
		arrowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		arrowPaint.setColor(getResources().getColor(R.color.sunshine_dark_blue));

		arcPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		arcPaint.setColor(getResources().getColor(R.color.grey));
		windSpeed = -1;
		arcAngle = (windSpeed / arcRange) * arcRange;
		arrowPath = new Path();

		arcRect = new RectF();
		setFocusable(true);
	}

	float instrumentRange = 13;
	float instrumentMin   = -1;
	float arcRange = 360;
	float arcStartAngle = 90;
	float arcDiameter;
	float arcAngle;
	RectF arcRect;
	float windSpeed;

	float centerX;
	float centerY;
	double arrowAngle;
	double arrowPointAngle = 0.125 * Math.PI;

	float windDirection = 0;
	float lineSize;
	float lineThickness;

	float arrowStartX;
	float arrowStartY;
	float arrowBaseX;
	float arrowBaseY;
	float arrowPointX;
	float arrowPointY;
	float arrowPointLeftX;
	float arrowPointLeftY;
	float arrowPointRightX;
	float arrowPointRightY;

	double directionToAngle(float direction)
	{
		return (direction/-180.0) * Math.PI + Math.PI;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		centerX = w / 2.0f;
		centerY = h / 2.0f;

		lineSize = w * (2f / 3f);
		// draw the arc in the middle between the line edge and the view edge
		arcDiameter = (w - lineSize)/2 + lineSize;
		lineThickness = (w / 100f) * 3f;
		arrowPaint.setStrokeWidth(lineThickness);


		calculateDrawPoints();
		updateArrowPath();
	}
	private void updateArrowPath()
	{
		arrowPath.reset();
		arrowPath.moveTo(arrowPointX, arrowPointY);
		arrowPath.lineTo(arrowPointLeftX, arrowPointLeftY);
		arrowPath.lineTo(arrowPointRightX,arrowPointRightY);
		arrowPath.lineTo(arrowPointX, arrowPointY);
	}

	private void calculateDrawPoints()
	{
		// Arrow draw points
		arrowAngle = directionToAngle(windDirection);
		float lineDiameter = lineSize / 2f;
		float arrowXDiff = (float) (lineDiameter * Math.sin(arrowAngle));
		float arrowYDiff = (float) (lineDiameter * Math.cos(arrowAngle));

		arrowStartX = centerX - arrowXDiff;
		arrowPointX = centerX + arrowXDiff;

		arrowStartY = centerY - arrowYDiff;
		arrowPointY = centerY + arrowYDiff;

		float arrowPointSize = lineSize / 4f;
		float arrowPointXDiff = (float) (arrowPointSize * Math.sin(arrowAngle + arrowPointAngle - Math.PI));
		float arrowPointYDiff = (float) (arrowPointSize * Math.cos(arrowAngle + arrowPointAngle - Math.PI));

		arrowPointLeftX = arrowPointX + arrowPointXDiff;
		arrowPointLeftY = arrowPointY + arrowPointYDiff;

		arrowPointXDiff = (float) (arrowPointSize * Math.sin(arrowAngle - arrowPointAngle + Math.PI));
		arrowPointYDiff = (float) (arrowPointSize * Math.cos(arrowAngle - arrowPointAngle + Math.PI));
		arrowPointRightX = arrowPointX + arrowPointXDiff;
		arrowPointRightY = arrowPointY + arrowPointYDiff;

		float arrowBaseXDiff = (float) ((lineSize - arrowPointSize)* Math.sin(arrowAngle));
		float arrowBaseYDiff = (float) ((lineSize - arrowPointSize) * Math.cos(arrowAngle));
		arrowBaseX = arrowStartX+arrowBaseXDiff;
		arrowBaseY = arrowStartY+arrowBaseYDiff;

		// Arc draw points
		float arcRadius = (arcDiameter/2);
		arcRect.bottom = centerY + arcRadius;
		arcRect.top    = centerY - arcRadius;
		arcRect.left   = centerX - arcRadius;
		arcRect.right  = centerX + arcRadius;

		arcAngle = ((windSpeed - instrumentMin) / instrumentRange) * arcRange;
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
		canvas.drawArc(arcRect, arcStartAngle, arcAngle, true, arcPaint);
		canvas.drawLine(arrowStartX, arrowStartY, arrowBaseX, arrowBaseY, arrowPaint);
		canvas.drawPath(arrowPath, arrowPaint);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		int heightMode 	= MeasureSpec.getMode(heightMeasureSpec);
		int height 		= MeasureSpec.getSize(heightMeasureSpec);
		int widthMode 	= MeasureSpec.getMode(widthMeasureSpec);
		int width 		= MeasureSpec.getSize(widthMeasureSpec);
		int wantedWidth = 100;
		int wantedHeight = 100;
		switch (heightMode)
		{
			case MeasureSpec.AT_MOST:
				if (wantedHeight <= height)
					break;
			case MeasureSpec.EXACTLY:
					wantedHeight = height;

		}
		switch (widthMode)
		{
			case MeasureSpec.AT_MOST:
				if (wantedWidth <= width)
					break;
			case MeasureSpec.EXACTLY:
				wantedWidth = width;

		}
		// force square dimensions
		if (wantedHeight < wantedWidth)
			wantedHeight = wantedWidth;
		if (wantedWidth < height)
			wantedWidth = wantedHeight;
		setMeasuredDimension(wantedWidth,wantedHeight);
	}
	private float convertSpeedToBaufort(float speed)
	{
		if(speed >= 117.4)
			return 12;
		if(speed >= 102.4)
			return 11;
		if(speed >= 88.1)
			return 10;
		if(speed >= 74.6)
			return 9;
		if(speed >= 61.8)
			return 8;
		if(speed >= 49.9)
			return 7;
		if(speed >= 38.8)
			return 6;
		if(speed >= 28.7)
			return 5;
		if (speed >= 19.7)
			return 4;
		if(speed >= 11.9)
			return 3;
		if(speed >= 2.2)
			return 2;
		if(speed >=1.1)
			return 1;
		return 0;
	}
	public void setWindSpeed(float newSpeed)
	{
		newSpeed = convertSpeedToBaufort(newSpeed);
		if(windSpeed != newSpeed)
		{
			windSpeed = newSpeed;
			calculateDrawPoints();
			updateArrowPath();
			invalidate();
		}

	}

	@Override
	public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent event)
	{
		String talkBackString = getContext().getString(R.string.format_wind_angle,windDirection);
		if(talkBackString == null)
			talkBackString = Float.toString(windDirection);
		event.getText().add(talkBackString);
		return true;
	}

	/**
	 * Sets wind direction 0 being North, rotating clockwise
	 * @param newDirection direction in degrees.
	 */
	public void setWindDirection(float newDirection)
	{
		if(windDirection != newDirection)
		{
			windDirection = newDirection;
			calculateDrawPoints();
			updateArrowPath();
			invalidate();
			triggerAccessabilityUpdate();
		}

	}

	private void triggerAccessabilityUpdate()
	{
		Context context = getContext();
		AccessibilityManager manager =	(AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
		if(manager != null && manager.isEnabled())
			sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED);
	}
}
