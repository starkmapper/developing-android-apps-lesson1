package nl.mapper.sunshine;

import org.json.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by mwstappe on 21-1-2015.
 */
public class OpenWeatherJsonParser
{
	public static final String JSON_DAYS="list";
	public static final String JSON_TEMP="temp";
	public static final String JSON_MAX="max";
	public static double getMaxTemperatureForDay(String weatherJsonStr,int dayIndex) throws JSONException
	{
		JSONObject jsonObj = new JSONObject(weatherJsonStr);
		JSONArray forecasts = jsonObj.getJSONArray(JSON_DAYS);
		JSONObject dayForecast = forecasts.getJSONObject(dayIndex);
		JSONObject dayTempInfo = dayForecast.getJSONObject(JSON_TEMP);
		return dayTempInfo.getDouble(JSON_MAX);
	}

	/* The date/time conversion code is going to be moved outside the asynctask later,
	 * so for convenience we're breaking it out into its own method now.
	 */
	private static String getReadableDateString(long time){
		// Because the API returns a unix timestamp (measured in seconds),
		// it must be converted to milliseconds in order to be converted to valid date.
		Date date = new Date(time * 1000);
		SimpleDateFormat format = new SimpleDateFormat("E, MMM d");
		return format.format(date);
	}

	/**
	 * Prepare the weather high/lows for presentation.
	 */
	private static String formatHighLows(double high, double low) {
		// For presentation, assume the user doesn't care about tenths of a degree.
		return  Math.round(high) + "/" + Math.round(low);
	}
														   /**
														 * Take the String representing the complete forecast in JSON Format and
														 * pull out the data we need to construct the Strings needed for the wireframes.
														 *
														 * Fortunately parsing is easy:  constructor takes the JSON string and converts it
														 * into an Object hierarchy for us.
														 */
	public static ArrayList<String> getWeatherDataFromJson(String forecastJsonStr, boolean Imperial)
			throws JSONException {

		// These are the names of the JSON objects that need to be extracted.
		final String OWM_LIST = "list";
		final String OWM_WEATHER = "weather";
		final String OWM_TEMPERATURE = "temp";
		final String OWM_MAX = "max";
		final String OWM_MIN = "min";
		final String OWM_DATETIME = "dt";
		final String OWM_DESCRIPTION = "main";

		JSONObject forecastJson = new JSONObject(forecastJsonStr);
		JSONArray weatherArray = forecastJson.getJSONArray(OWM_LIST);

		ArrayList<String> resultStrs = new ArrayList<String>();
		for(int i = 0; i < weatherArray.length(); i++) {
			// For now, using the format "Day, description, hi/low"
			String day;
			String description;
			String highAndLow;

			// Get the JSON object representing the day
			JSONObject dayForecast = weatherArray.getJSONObject(i);

			// The date/time is returned as a long.  We need to convert that
			// into something human-readable, since most people won't read "1400356800" as
			// "this saturday".
			long dateTime = dayForecast.getLong(OWM_DATETIME);
			day = getReadableDateString(dateTime);

			// description is in a child array called "weather", which is 1 element long.
			JSONObject weatherObject = dayForecast.getJSONArray(OWM_WEATHER).getJSONObject(0);
			description = weatherObject.getString(OWM_DESCRIPTION);

			// Temperatures are in a child object called "temp".  Try not to name variables
			// "temp" when working with temperature.  It confuses everybody.
			JSONObject temperatureObject = dayForecast.getJSONObject(OWM_TEMPERATURE);
			double high = temperatureObject.getDouble(OWM_MAX);
			double low = temperatureObject.getDouble(OWM_MIN);
			if(Imperial)
			{
				high = CtoF(high);
				low = CtoF(low);
			}

			highAndLow = formatHighLows(high, low);
			resultStrs.add( day + " - " + description + " - " + highAndLow);
		}

		return resultStrs;
	}
	private static double CtoF(double f)
	{
		return (f * 1.8) + 32;
	}



}
