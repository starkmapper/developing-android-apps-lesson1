package nl.mapper.sunshine;

import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import nl.mapper.sunshine.data.WeatherContract;
import nl.mapper.sunshine.sync.SunshineSyncAdapter;


public class MainActivity extends ActionBarActivity implements ForecastFragment.Callback
{
	@Override
	public void onItemSelected(Uri dateUri)
	{
		if(mTwoPane)
		{
			DetailFragment df = (DetailFragment) getSupportFragmentManager().findFragmentByTag(DETAILFRAGMENT_TAG);
			if(df!=null)
				df.setWeatherUri(dateUri);
		}
		else
		{
			Intent detailIntent = new Intent(this, DetailActivity.class);
			detailIntent.setData(dateUri);
			startActivity(detailIntent);
		}
	}

	private  static final String DETAILFRAGMENT_TAG = "DFTAG";

	@Override
	protected void onResume()
	{
		super.onResume();
		String newLocation = Utility.getPreferredLocation(this);
		if (currentLocationSetting == null ||
				(newLocation != null && !newLocation.equals(currentLocationSetting))
				)
		{
			currentLocationSetting = newLocation;
			ForecastFragment ff = (ForecastFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_forecast);
			if (ff != null)
				ff.onLocationChanged();
			DetailFragment df = (DetailFragment)getSupportFragmentManager().findFragmentByTag(DETAILFRAGMENT_TAG);
			if(df!=null)
				df.onLocationChanged(currentLocationSetting);
		}

	}
	private final String LOG_TAG = MainActivity.class.getSimpleName();
	private boolean mTwoPane;

@Override
  protected void onCreate(Bundle savedInstanceState)
  {

    super.onCreate(savedInstanceState);
	currentLocationSetting = Utility.getPreferredLocation(this);
    setContentView(R.layout.activity_main);
	mTwoPane = findViewById(R.id.weather_detail_container) != null;

    if (savedInstanceState == null && mTwoPane)
    {
		DetailFragment df;
		if (currentLocationSetting != null && currentLocationSetting.length() > 0)
		{
			Uri forecastUri = WeatherContract.WeatherEntry.buildWeatherLocationWithDate(currentLocationSetting, Utility.getCurrentUTCDate());
			df = DetailFragment.createInstance(forecastUri);
		}
		else
		{
			df = new DetailFragment();
		}
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.weather_detail_container, df, DETAILFRAGMENT_TAG)
				.commit();
    }
	ForecastFragment ff = (ForecastFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_forecast);
	  if(ff !=null)
		  ff.setTodayViewType(!mTwoPane);
	if(!mTwoPane)
		getSupportActionBar().setElevation(0f);

	  SunshineSyncAdapter.initializeSyncAdapter(this);
  }
  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
	  return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.

    switch (item.getItemId())
    {
		case R.id.action_settings:
			Intent settingsIntent = new Intent(this, SettingsActivity.class);
			startActivity(settingsIntent);
			return true;
		case R.id.action_view_location:
			Intent mapIntent = new Intent(Intent.ACTION_VIEW);
			mapIntent.setData(Uri.parse("geo:0,0?q="+ PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.pref_location_key), getString(R.string.pref_location_default))));
			// check if mapping application exists using resolveActivity
			if(mapIntent.resolveActivity(getPackageManager()) != null)
				startActivity(mapIntent);
			return true;


    }

    return super.onOptionsItemSelected(item);
  }
	private String currentLocationSetting;
}
