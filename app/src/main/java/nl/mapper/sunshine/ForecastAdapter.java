	package nl.mapper.sunshine;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import nl.mapper.sunshine.data.WeatherContract;

import nl.mapper.sunshine.R;

/**
 * {@link ForecastAdapter} exposes a list of weather forecasts
 * from a {@link android.database.Cursor} to a {@link android.widget.ListView}.
 */
public class ForecastAdapter extends CursorAdapter {
    public ForecastAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    /**
     * Prepare the weather high/lows for presentation.
     */
    private String formatHighLows(double high, double low) {
        boolean isMetric = Utility.isMetric(mContext);
        String highLowStr = Utility.formatTemperature(mContext,high, isMetric) + "/" + Utility.formatTemperature(mContext, low, isMetric);
        return highLowStr;
    }
	private boolean mUseToday;

	public void setTodayViewType(boolean useTodayView)
	{
		mUseToday = useTodayView;
	}

    /*
        This is ported from FetchWeatherTask --- but now we go straight from the cursor to the
        string.
     */
    private String convertCursorRowToUXFormat(Cursor cursor) {
        String highAndLow = formatHighLows(
                cursor.getDouble(ForecastFragment.COL_WEATHER_MAX_TEMP),
                cursor.getDouble(ForecastFragment.COL_WEATHER_MIN_TEMP));

        return Utility.formatDate(cursor.getLong(ForecastFragment.COL_WEATHER_DATE)) +
                " - " + cursor.getString(ForecastFragment.COL_WEATHER_DESC) +
                " - " + highAndLow;
    }
	private static final int TODAY_VIEW_TYPE=0;
	private static final int FUTURE_VIEW_TYPE=1;

	@Override
	public int getViewTypeCount()
	{
		return 2;
	}

	@Override
	public int getItemViewType(int position)
	{
		return position == 0 && mUseToday ? TODAY_VIEW_TYPE : FUTURE_VIEW_TYPE;
	}

	/*
			Remember that these views are reused as needed.
		 */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
		int ViewType = getItemViewType(cursor.getPosition());
		int inflateID = ViewType == TODAY_VIEW_TYPE ? R.layout.list_item_forecast_today : R.layout.list_item_forecast;
        View view = LayoutInflater.from(context).inflate(inflateID, parent, false);
		ViewHolder holder = new ViewHolder(view);
		view.setTag(holder);

        return view;
    }

    /*
        This is where we fill-in the views with the contents of the cursor.
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Set weather icon
		int weatherID = cursor.getInt(ForecastFragment.COL_WEATHER_CONDITION_ID);
		ViewHolder holder = (ViewHolder) view.getTag();
		int dsplayType= getItemViewType(cursor.getPosition());
		int weatherIconId;
		String dateString = Utility.getFriendlyDayString(context, cursor.getLong(ForecastFragment.COL_WEATHER_DATE));
		if (dsplayType == TODAY_VIEW_TYPE)
		{
			weatherIconId = Utility.getArtResourceForWeatherCondition(weatherID);
			dateString += ", " +Utility.getFormattedMonthDay(context, cursor.getLong(ForecastFragment.COL_WEATHER_DATE));
		}
		else
			weatherIconId = Utility.getIconResourceForWeatherCondition(weatherID);

		holder.date.setText(dateString);
		holder.icon.setImageResource(weatherIconId);


		String forecastString = cursor.getString(ForecastFragment.COL_WEATHER_DESC);
		holder.forecast.setText(forecastString);

		String maxString = Utility.formatTemperature(context,
				cursor.getDouble(ForecastFragment.COL_WEATHER_MAX_TEMP),
				Utility.isMetric(context));
		holder.max.setText(maxString);

		String minString = Utility.formatTemperature(context,
						cursor.getDouble(ForecastFragment.COL_WEATHER_MIN_TEMP),
						Utility.isMetric(context));
		holder.min.setText(minString);

		String ContentString = context.getString(R.string.format_accessability_listItem,dateString, forecastString, maxString,minString);
		view.setContentDescription(ContentString);
	}
	class ViewHolder
	{
		public final ImageView icon;
		public final TextView forecast;
		public final TextView date;
		public final TextView min;
		public final TextView max;

		ViewHolder(View view)
		{
			forecast = (TextView)view.findViewById(R.id.list_item_forecast_textview);
			icon = (ImageView)view.findViewById(R.id.list_item_icon);
			date = (TextView)view.findViewById(R.id.list_item_date_textview);
			min = (TextView)view.findViewById(R.id.list_item_low_textview);
			max = (TextView)view.findViewById(R.id.list_item_high_textview);
		}

	}
}