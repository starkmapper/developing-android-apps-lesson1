package nl.mapper.sunshine;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.CursorLoader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;


import nl.mapper.sunshine.data.WeatherContract;
import nl.mapper.sunshine.sync.SunshineSyncAdapter;

/**
 * Fragment displaying list of forecasts for the next 14 days
 */
public class ForecastFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

	/**
	 * A callback interface that all activities containing this fragment must
	 * implement. This mechanism allows activities to be notified of item
	 * selections.
	 */
	public interface Callback {
		/**
		 * DetailFragmentCallback for when an item has been selected.
		 */
		public void onItemSelected(Uri dateUri);
	}

	boolean mUseToday;

	/**
	 * Controls weather to use a differently styled view for today in the forecast list
	 * @param useTodayView true to use a differently styled view for today's weather
	 */
	public void setTodayViewType(boolean useTodayView)
	{
		mUseToday = useTodayView;
		if (adapter!=null)
			adapter.setTodayViewType(useTodayView);
	}
	public ForecastFragment(){}
    private static String TAG = "Sunshine - main";
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	private ForecastAdapter adapter;
	public static final int WEATHER_LOADER_ID = 12345;
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		getLoaderManager().initLoader(WEATHER_LOADER_ID,savedInstanceState, this);
	}

	private static final String[] FORECAST_COLUMNS = {
			// In this case the id needs to be fully qualified with a table name, since
			// the content provider joins the location & weather tables in the background
			// (both have an _id column)
			// On the one hand, that's annoying.  On the other, you can search the weather table
			// using the location set by the user, which is only in the Location table.
			// So the convenience is worth it.
			WeatherContract.WeatherEntry.TABLE_NAME + "." + WeatherContract.WeatherEntry._ID,
			WeatherContract.WeatherEntry.COLUMN_DATE,
			WeatherContract.WeatherEntry.COLUMN_SHORT_DESC,
			WeatherContract.WeatherEntry.COLUMN_MAX_TEMP,
			WeatherContract.WeatherEntry.COLUMN_MIN_TEMP,
			WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING,
			WeatherContract.WeatherEntry.COLUMN_WEATHER_ID,
			WeatherContract.LocationEntry.COLUMN_COORD_LAT,
			WeatherContract.LocationEntry.COLUMN_COORD_LONG
	};

	// These indices are tied to FORECAST_COLUMNS.  If FORECAST_COLUMNS changes, these
	// must change.
	static final int COL_WEATHER_ID = 0;
	static final int COL_WEATHER_DATE = 1;
	static final int COL_WEATHER_DESC = 2;
	static final int COL_WEATHER_MAX_TEMP = 3;
	static final int COL_WEATHER_MIN_TEMP = 4;
	static final int COL_LOCATION_SETTING = 5;
	static final int COL_WEATHER_CONDITION_ID = 6;
	static final int COL_COORD_LAT = 7;
	static final int COL_COORD_LONG = 8;

	public void onLocationChanged()
	{
		getLoaderManager().restartLoader(WEATHER_LOADER_ID,null,this);
	}
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args)
	{

		String locationSetting = Utility.getPreferredLocation(getActivity());
		if(locationSetting!= null && locationSetting.length() > 0)
		{
			// Sort weather entries ascending by date
			String sortString = WeatherContract.WeatherEntry.COLUMN_DATE + " ASC";
			Uri forecastUri = WeatherContract.WeatherEntry.buildWeatherLocationWithStartDate(locationSetting, System.currentTimeMillis());
			return new CursorLoader(getActivity(), forecastUri, FORECAST_COLUMNS, null, null, sortString);
		}

		return null;
	}

	@Override
	public void onStart()
	{
		super.onStart();
		//getWeatherUpdate();
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader)
	{
		adapter.swapCursor(null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data)
	{
		adapter.swapCursor(data);
		ListView list;
		list = (ListView) getView().findViewById(R.id.listview_forecast);
		if (list!=null)
		{
			// che
			if(scrollPosition != ListView.INVALID_POSITION)
				list.smoothScrollToPosition(scrollPosition);
			else
				list.setItemChecked(0, true);

		}
		if(data.moveToFirst())
		{
			locationSetting = data.getString(COL_LOCATION_SETTING);
			latitude = data.getString(COL_COORD_LAT);
			longitude		= data.getString(COL_COORD_LONG);

		}
	}

	private static final String STATE_POSITION = "ScrollPosition";
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		ListView list = (ListView)getView().findViewById( R.id.listview_forecast);
		if(list!=null)
		{
			int position = list.getCheckedItemPosition();
			outState.putInt(STATE_POSITION, position);
		}
		super.onSaveInstanceState(outState);
	}

	void notifyParentOfItemSelection(AdapterView parent, int position)
	{
		Cursor itemCursor = (Cursor) parent.getItemAtPosition(position);
		if(itemCursor != null)
		{
			String locationSetting = Utility.getPreferredLocation(getActivity());
			Uri weatherUri = WeatherContract.WeatherEntry.buildWeatherLocationWithDate(locationSetting, itemCursor.getLong(COL_WEATHER_DATE));
			Callback callback = (Callback)getActivity();
			callback.onItemSelected(weatherUri);
		}
	}
	int scrollPosition = ListView.INVALID_POSITION;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
		adapter = new ForecastAdapter(getActivity(),null,0);
		adapter.setTodayViewType(mUseToday);
		ListView list = (ListView)rootView.findViewById( R.id.listview_forecast);
        list.setAdapter(adapter);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView parent, View view, int position, long id)
			{
				notifyParentOfItemSelection(parent,position);
			}
		});
		if(savedInstanceState != null && savedInstanceState.containsKey(STATE_POSITION))
			scrollPosition = savedInstanceState.getInt(STATE_POSITION);


        return rootView;
    }

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu,inflater);
		inflater.inflate(R.menu.forecastfragment,menu);

	}
	String latitude;
	String longitude;
	String locationSetting;
	@Override
	public boolean onOptionsItemSelected (MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.action_view_location:
				Intent mapIntent = new Intent(Intent.ACTION_VIEW);

				mapIntent.setData(Uri.parse("geo:0,0?q="+ latitude +","+longitude+"("+locationSetting+")"));
				// check if mapping application exists using resolveActivity
				if(mapIntent.resolveActivity(getActivity().getPackageManager()) != null)
					startActivity(mapIntent);
				return true;

		}
		return false;

	}
	private void getWeatherUpdate()
	{
		SunshineSyncAdapter.syncImmediately(getActivity());
	}



}
