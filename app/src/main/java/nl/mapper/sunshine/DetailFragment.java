package nl.mapper.sunshine;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import nl.mapper.sunshine.data.WeatherContract;
import nl.mapper.sunshine.view.ArcView;

/**
 * Fragment actually showing the weater detail
 */
public class DetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>
{
	private static final String HASHTAG = " #SunshineApp";
	public void setWeatherUri(Uri newUri)
	{
		weatherUri = newUri;
		getLoaderManager().restartLoader(WEATHER_LOADER_ID,null,this);
	}

	private static final String URI_KEY = "URI";
	public static DetailFragment createInstance(Uri dateUri)
	{
		Bundle data = new Bundle();
		data.putString(URI_KEY, dateUri.toString());
		DetailFragment df = new DetailFragment();
		df.setArguments(data);
		return df;
	}

	public DetailFragment()
	{
		setHasOptionsMenu(true);
	}
	public static final int WEATHER_LOADER_ID = 1235;

	ViewHolder viewHolder;
	public void onLocationChanged(String newLocation)
	{
		if (weatherUri != null)
		{
			long date = WeatherContract.WeatherEntry.getDateFromUri(weatherUri);
			weatherUri = WeatherContract.WeatherEntry.buildWeatherLocationWithDate(newLocation,date);
			getLoaderManager().restartLoader(WEATHER_LOADER_ID,null,this);
		}

	}

	private Uri getUriArgument()
	{
		Bundle args = getArguments();
		if(args != null)
			return Uri.parse(args.getString(URI_KEY));
		return null;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		weatherUri = getUriArgument();
		getLoaderManager().initLoader(WEATHER_LOADER_ID,savedInstanceState, this);
		mContext = getActivity();
	}

	private Uri weatherUri;
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args)
	{
		if(weatherUri != null)
		{
			String sortString = WeatherContract.WeatherEntry.COLUMN_DATE + " ASC";
			return new CursorLoader(getActivity(), weatherUri, FORECAST_COLUMNS, null, null, sortString);
		}
		return null;
	}
	String detailString;
	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data)
	{
		weatherData = data;
		if(data.moveToFirst())
		{
			detailString = convertCursorRowToUXFormat(data);
		}
		setDetailText();
		setShareAction();
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader)
	{
		weatherData = null;
	}

	/**
	 * Prepare the weather high/lows for presentation.
	 */
	private String formatHighLows(double high, double low) {
		boolean isMetric = Utility.isMetric(getActivity());
		return  Utility.formatTemperature(getActivity(), high, isMetric) + "/" + Utility.formatTemperature(getActivity(), low, isMetric);
	}


	/*
		This is ported from FetchWeatherTask --- but now we go straight from the cursor to the
		string.
	 */
	private String convertCursorRowToUXFormat(Cursor cursor) {
		String highAndLow = formatHighLows(
				cursor.getDouble(COL_WEATHER_MAX_TEMP),
				cursor.getDouble(COL_WEATHER_MIN_TEMP));

		return Utility.formatDate(cursor.getLong(COL_WEATHER_DATE)) +
				" - " + cursor.getString(COL_WEATHER_DESC) +
				" - " + highAndLow;
	}

	private static final String[] FORECAST_COLUMNS = {
			// In this case the id needs to be fully qualified with a table name, since
			// the content provider joins the location & weather tables in the background
			// (both have an _id column)
			// On the one hand, that's annoying.  On the other, you can search the weather table
			// using the location set by the user, which is only in the Location table.
			// So the convenience is worth it.
			WeatherContract.WeatherEntry.TABLE_NAME + "." + WeatherContract.WeatherEntry._ID,
			WeatherContract.WeatherEntry.COLUMN_DATE,
			WeatherContract.WeatherEntry.COLUMN_SHORT_DESC,
			WeatherContract.WeatherEntry.COLUMN_MAX_TEMP,
			WeatherContract.WeatherEntry.COLUMN_MIN_TEMP,
			WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING,
			WeatherContract.WeatherEntry.COLUMN_WEATHER_ID,
			WeatherContract.LocationEntry.COLUMN_COORD_LAT,
			WeatherContract.LocationEntry.COLUMN_COORD_LONG,
			WeatherContract.WeatherEntry.COLUMN_HUMIDITY,
			WeatherContract.WeatherEntry.COLUMN_PRESSURE,
			WeatherContract.WeatherEntry.COLUMN_WIND_SPEED,
			WeatherContract.WeatherEntry.COLUMN_DEGREES,



	};

	// These indices are tied to FORECAST_COLUMNS.  If FORECAST_COLUMNS changes, these
	// must change.
	static final int COL_WEATHER_ID = 0;
	static final int COL_WEATHER_DATE = 1;
	static final int COL_WEATHER_DESC = 2;
	static final int COL_WEATHER_MAX_TEMP = 3;
	static final int COL_WEATHER_MIN_TEMP = 4;
	static final int COL_LOCATION_SETTING = 5;
	static final int COL_WEATHER_CONDITION_ID = 6;
	static final int COL_COORD_LAT = 7;
	static final int COL_COORD_LONG = 8;
	static final int COL_HUMIDITY = 9;
	static final int COL_PRESSURE = 10;
	static final int COL_WIND_SPEED = 11;
	static final int COL_WIND_DIRECTION = 12;

	private Cursor weatherData;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
		viewHolder = new ViewHolder(rootView);
		rootView.setTag(viewHolder);
		setDetailText();
		return rootView;
	}
	Context mContext;

	private void setDetailText()
	{
		if(weatherData != null && viewHolder!= null && weatherData.moveToFirst())
		{
			int  weatherIconId = Utility.getArtResourceForWeatherCondition(weatherData.getInt(COL_WEATHER_CONDITION_ID));

			viewHolder.icon.setImageResource(weatherIconId);

			viewHolder.forecast.setText(weatherData.getString(COL_WEATHER_DESC));

			viewHolder.day.setText(Utility.getDayName(mContext, weatherData.getLong(COL_WEATHER_DATE)));

			viewHolder.date.setText(Utility.getFormattedMonthDay(mContext, weatherData.getLong(COL_WEATHER_DATE)));

			String maxString = Utility.formatTemperature(mContext,
							weatherData.getDouble(COL_WEATHER_MAX_TEMP),
							Utility.isMetric(mContext));
			viewHolder.max.setText(maxString);
			viewHolder.max.setContentDescription(getString(R.string.format_accessability_maxTemp,maxString));

			String minString = Utility.formatTemperature(mContext,
					weatherData.getDouble(COL_WEATHER_MIN_TEMP),
					Utility.isMetric(mContext));
			viewHolder.min.setText(minString);
			viewHolder.min.setContentDescription(getString(R.string.format_accessability_minTemp, minString));

			float humidity = weatherData.getFloat(COL_HUMIDITY);
			viewHolder.humidity.setText(String.format(getString(R.string.format_humidity), humidity));

			float pressure = weatherData.getFloat(COL_PRESSURE);
			viewHolder.pressure.setText(String.format(getString(R.string.format_pressure), pressure));

			float speed = weatherData.getFloat(COL_WIND_SPEED);
			float direction = weatherData.getFloat(COL_WIND_DIRECTION);

			ArcView windView = (ArcView)getView().findViewById(R.id.detail_wind_direction_and_speed);
			windView.setWindDirection(direction);
			windView.setWindSpeed(speed);

			viewHolder.wind.setText(Utility.getFormattedWind(mContext,speed,direction));

		}
	}

	private ShareActionProvider shareProvider;
	private boolean shareActionSet = false;
	private void setShareAction()
	{
		if (!shareActionSet && shareProvider!= null && detailString != null)
		{
			shareProvider.setShareIntent(createShareIntent(detailString + HASHTAG));
			shareActionSet = true;
		}
	}
	private Intent createShareIntent(String message)
	{
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
		shareIntent.setType("text/plain");
		shareIntent.putExtra(Intent.EXTRA_TEXT, message);
		return shareIntent;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		inflater.inflate(R.menu.detailfragment, menu);
		shareProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menu.findItem(R.id.action_share_item));
		if(detailString!=null)
		{
			setShareAction();
		}
		super.onCreateOptionsMenu(menu, inflater);
	}

	class ViewHolder
	{
		public final ImageView icon;
		public final TextView forecast;
		public final TextView date;
		public final TextView day;
		public final TextView min;
		public final TextView max;
		public final TextView humidity;
		public final TextView wind;
		public final TextView pressure;

		ViewHolder(View view)
		{
			forecast = (TextView)view.findViewById(R.id.detail_forecast);
			icon = (ImageView)view.findViewById(R.id.detail_icon);
			date = (TextView)view.findViewById(R.id.detail_date_textview);
			day = (TextView)view.findViewById(R.id.detail_day_textview);
			min = (TextView)view.findViewById(R.id.detail_low_textview);
			max = (TextView)view.findViewById(R.id.detail_high_textview);
			wind = (TextView)view.findViewById(R.id.detail_wind);
			humidity = (TextView)view.findViewById(R.id.detail_humidity);
			pressure = (TextView)view.findViewById(R.id.detail_pressure);
		}

	}

}
